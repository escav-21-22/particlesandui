using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Incrementer : MonoBehaviour
{
    /// <summary>
    /// Referencia al componente Text del contador correspondiente
    /// </summary>
    public Text counterText;
    /// <summary>
    /// Valor inicial que se desea para el contador
    /// </summary>
    public int initial;

    /// <summary>
    /// Contador interno 
    /// </summary>
    private int count;

    // Start is called before the first frame update
    void Start()
    {
        // Asigna el valor inicial al contador
        count = initial;

        // Muestra el contador en la interfaz, con el par�metro text y transformando la variable entera en string con el m�todo ToString
        counterText.text = count.ToString();
    }

    // Funci�n para incrementar el contador correspondiente.
    // Debe ser p�blica y asignarse en el evento OnClick del bot�n correspondiente (desde el editor).
    // Se ha utilizado una variable entera como par�metro para poder modificar el incremento desde el editor
    public void Increment(int incr) {
        // Incrementa el contador seg�n lo indicado en el par�metro de la funci�n
        count += incr;

        // Muestra el contador en la interfaz, con el par�metro text y transformando la variable entera en string con el m�todo ToString
        counterText.text = count.ToString();
    }
}