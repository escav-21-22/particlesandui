using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireParticlesController : MonoBehaviour
{
    [Header("Particle Systems")]
    public ParticleSystem fireParent;
    public ParticleSystem sparks;
    public ParticleSystem fire;
    public ParticleSystem smoke;
    public ParticleSystem fireDark;

    [Header("UI")]
    public Text playStopButtonText;

    [Header("Blue Colors")]
    public Color fireParentBlueColor;
    public Color sparksBlueColor1;
    public Color sparksBlueColor2;
    public Color fireBlueColor;
    public Color fireDarkBlueColor;

    [Header("Green Colors")]
    public Color fireParentGreenColor;
    public Color sparksGreenColor1;
    public Color sparksGreenColor2;
    public Color fireGreenColor;
    public Color fireDarkGreenColor;

    [Header("Red Colors")]
    public Color fireParentRedColor;
    public Color sparksRedColor1;
    public Color sparksRedColor2;
    public Color fireRedColor;
    public Color fireDarkRedColor;

    [Header("Yellow Colors")]
    public Color fireParentYellowColor;
    public Color sparksYellowColor1;
    public Color sparksYellowColor2;
    public Color fireYellowColor;
    public Color fireDarkYellowColor;

    [Header("Wind Amounts")]
    public float fireParentWindAmount;
    public float sparksWindAmount;
    public float fireWindAmount;
    public float smokeWindAmount;
    public float fireDarkWindAmount;

    // Almacena si los sistemas de part�culas est�n en ejecuci�n
    private bool playing;

    private void Start()
    {
        // Por defecto, el sistema de part�culas empieza en ejecuci�n
        playing = true;
    }

    public void PlayStop()
    {
        // Cambia el estado en que deben estar los sistemas de part�culas
        playing = !playing;

        // Comprueba si el sistema est� en ejecuci�n o no antes de modificar el texto del bot�n
        if (playing)
        {
            // Los sistemas de part�culas est�n en ejecuci�n, luego el bot�n es para detenerlos y se pone el texto "Stop"
            playStopButtonText.text = "Stop";
        } else
        {
            // Los sistemas de part�culas est�n detenidos, luego el bot�n es para ejecutarlos y se pone el texto "Play"
            playStopButtonText.text = "Play";
        }

        // Modifica el m�dulo de emisi�n para cada uno de los sistemas de part�culas relacionados
        var emission = fireParent.emission;
        emission.enabled = playing;

        emission = sparks.emission;
        emission.enabled = playing;

        emission = fire.emission;
        emission.enabled = playing;

        emission = smoke.emission;
        emission.enabled = playing;

        emission = fireDark.emission;
        emission.enabled = playing;

    }

    // Funci�n para poner el fuego de color azul.
    // Debe ser p�blica y asignarse en el evento OnClick del bot�n correspondiente (desde el editor).
    public void SetColorBlue()
    {
        // Accede al m�dulo principal de cada sistema y le modifica el par�metro startColor

        var main = fireParent.main;
        main.startColor = fireParentBlueColor;

        // En este caso el color es un gradiente, por lo que se necesita otra variable auxiliar y modificar colorMin y ColorMax 
        main = sparks.main;
        var auxColor = main.startColor;
        auxColor.colorMin = sparksBlueColor1;
        auxColor.colorMax = sparksBlueColor2;
        main.startColor = auxColor;

        main = fire.main;
        main.startColor = fireBlueColor;

        main = fireDark.main;
        main.startColor = fireDarkBlueColor;
    }

    // Funci�n para poner el fuego de color verde.
    // Debe ser p�blica y asignarse en el evento OnClick del bot�n correspondiente (desde el editor).
    public void SetColorGreen()
    {
        // Accede al m�dulo principal de cada sistema y le modifica el par�metro startColor

        var main = fireParent.main;
        main.startColor = fireParentGreenColor;

        // En este caso el color es un gradiente, por lo que se necesita otra variable auxiliar y modificar colorMin y ColorMax 
        main = sparks.main;
        var auxColor = main.startColor;
        auxColor.colorMin = sparksGreenColor1;
        auxColor.colorMax = sparksGreenColor2;
        main.startColor = auxColor;

        main = fire.main;
        main.startColor = fireGreenColor;

        main = fireDark.main;
        main.startColor = fireDarkGreenColor;
    }

    // Funci�n para poner el fuego de color rojo.
    // Debe ser p�blica y asignarse en el evento OnClick del bot�n correspondiente (desde el editor).
    public void SetColorRed()
    {
        // Accede al m�dulo principal de cada sistema y le modifica el par�metro startColor

        var main = fireParent.main;
        main.startColor = fireParentRedColor;

        // En este caso el color es un gradiente, por lo que se necesita otra variable auxiliar y modificar colorMin y ColorMax 
        main = sparks.main;
        var auxColor = main.startColor;
        auxColor.colorMin = sparksRedColor1;
        auxColor.colorMax = sparksRedColor2;
        main.startColor = auxColor;

        main = fire.main;
        main.startColor = fireRedColor;

        main = fireDark.main;
        main.startColor = fireDarkRedColor;
    }

    // Funci�n para poner el fuego de color amarillo.
    // Debe ser p�blica y asignarse en el evento OnClick del bot�n correspondiente (desde el editor).
    public void SetColorYellow()
    {
        // Accede al m�dulo principal de cada sistema y le modifica el par�metro startColor

        var main = fireParent.main;
        main.startColor = fireParentYellowColor;

        // En este caso el color es un gradiente, por lo que se necesita otra variable auxiliar y modificar colorMin y ColorMax 
        main = sparks.main;
        var auxColor = main.startColor;
        auxColor.colorMin = sparksYellowColor1;
        auxColor.colorMax = sparksYellowColor2;
        main.startColor = auxColor;

        main = fire.main;
        main.startColor = fireYellowColor;

        main = fireDark.main;
        main.startColor = fireDarkYellowColor;
    }

    // Funci�n para determinar la direcci�n en que afecta el viento al fuego.
    // Debe ser p�blica y asignarse en el evento OnValueChanges del slider correspondiente (desde el editor).
    // Debe contener un par�metro float dentro de la funci�n (en este caso de nombre windDirection). 
    public void SetWind(float windDirection)
    {
        // Accede al m�dulo de Force Over Lifetime de cada sistema y le modifica el par�metro forceOverTime en su valor X
        
        var forceOverTime = fireParent.forceOverLifetime;
        forceOverTime.x = windDirection * fireParentWindAmount;

        forceOverTime = sparks.forceOverLifetime;
        forceOverTime.x = windDirection * sparksWindAmount;

        forceOverTime = fire.forceOverLifetime;
        forceOverTime.x = windDirection * fireWindAmount;

        forceOverTime = smoke.forceOverLifetime;
        forceOverTime.x = windDirection * smokeWindAmount;

        forceOverTime = fireDark.forceOverLifetime;
        forceOverTime.x = windDirection * fireDarkWindAmount;
    }
}
