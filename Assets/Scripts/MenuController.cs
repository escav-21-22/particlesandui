using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

    // Funci�n para cargar la escena del sistema de part�culas de fuego.
    // Debe ser p�blica y asignarse en el evento OnClick del bot�n correspondiente (desde el editor).
    public void LoadFireScene() {
        // Carga la escena con el nombre "Fire", que debe estar incluida en el listado de escenas de Build Settings
        SceneManager.LoadScene("Fire");
    }

    // Funci�n para cargar la escena del men� inicial.
    // Debe ser p�blica y asignarse en el evento OnClick del bot�n correspondiente (desde el editor).
    public void LoadMenuScene()
    {
        // Carga la escena con el nombre "Menu", que debe estar incluida en el listado de escenas de Build Settings
        SceneManager.LoadScene("Menu");
    }

    // Funci�n para cargar la escena de la interfaz responsive.
    // Debe ser p�blica y asignarse en el evento OnClick del bot�n correspondiente (desde el editor).
    public void LoadUIScene()
    {
        // Carga la escena con el nombre "Interface", que debe estar incluida en el listado de escenas de Build Settings
        SceneManager.LoadScene("Interface");
    }

    // Funci�n para cerrar el visor de part�culas.
    // Debe ser p�blica y asignarse en el evento OnClick del bot�n correspondiente (desde el editor).
    public void QuitGame()
    {
        // Ordena a la aplicaci�n que se cierre
        Application.Quit();
    }
}
